using GpioTool.Models;
using GpioTool.Services;
using Microsoft.AspNetCore.Mvc;

namespace GpioTool.Controllers
{
    public class HomeController : Controller
    {
        private readonly IGpioService gpio;

        public HomeController(IGpioService gpio)
        {
            this.gpio = gpio;
        }

        [HttpGet]
        public IActionResult Index()
        {
            return View();
        }

        [HttpGet]
        public IActionResult Pins()
        {
            return Json(gpio.Pins);
        }

        [HttpPost]
        public IActionResult Write([FromBody] WriteModel model)
        {
            gpio.Write(model.Pin, model.Value);
            return Json(gpio.Pins);
        }
    }
}