new Vue({
    el: '#app',
    data: {
        busy: false,
        errorMessage: null,
        pins: {}
    },
    beforeMount() {
        this.update();
    },
    methods: {
        update() {
            this.busy = true;

            fetch('/Home/Pins', {
                headers: {
                    'Content-Type': 'application/json'
                }
            }).then(this.handleSuccess).catch(this.handleError);
        },
        toggle(pin) {
            this.busy = true;

            const body = {
                pin: parseInt(pin, 10),
                value: this.pins[pin] ? 0 : 1
            };

            fetch('/Home/Write', {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify(body)
            }).then(this.handleSuccess).catch(this.handleError);
        },
        async handleSuccess(response) {
            if (!response.ok) {
                this.handleError(new Error(`An error occured while fetching data: ${response.statusText}`));
                return;
            }

            this.errorMessage = null;
            this.pins = await response.json();
            this.busy = false;
        },
        handleError(error) {
            this.errorMessage = error.message;
            this.pins = {};
            this.busy = false;
        },
        classes(value) {
            return value ? 'btn-success' : 'btn-danger';
        }
    }
});
