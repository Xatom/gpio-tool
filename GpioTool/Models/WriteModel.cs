using System.Device.Gpio;
using System.Diagnostics.CodeAnalysis;

namespace GpioTool.Models
{
    public class WriteModel
    {
        [SuppressMessage("ReSharper", "UnusedAutoPropertyAccessor.Global")]
        public int Pin { get; set; }

        [SuppressMessage("ReSharper", "UnusedAutoPropertyAccessor.Global")]
        public PinValue Value { get; set; }
    }
}