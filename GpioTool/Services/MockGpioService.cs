using System.Collections.Generic;
using System.Device.Gpio;
using Microsoft.Extensions.Logging;

namespace GpioTool.Services
{
    public class MockGpioService : IGpioService
    {
        private readonly ILogger logger;

        private readonly Dictionary<int, PinValue> pins = new Dictionary<int, PinValue>
        {
            [1] = PinValue.Low,
            [2] = PinValue.Low,
            [3] = PinValue.Low,
            [4] = PinValue.Low,
            [5] = PinValue.Low
        };

        public MockGpioService(ILogger<MockGpioService> logger)
        {
            this.logger = logger;
        }

        public IReadOnlyDictionary<int, PinValue> Pins => pins;

        public void Dispose() => logger.LogDebug("Disposing");

        public void Write(int pin, PinValue value)
        {
            logger.LogInformation($"Setting pin #{pin} to {value}");
            pins[pin] = value;
        }
    }
}