using System.Collections.Generic;
using System.Device.Gpio;
using System.Device.Gpio.Drivers;
using Microsoft.Extensions.Logging;

namespace GpioTool.Services
{
    public class PiGpioService : IGpioService
    {
        private readonly ILogger logger;

        private readonly GpioController controller;

        private readonly Dictionary<int, PinValue> pins = new Dictionary<int, PinValue>
        {
            [5] = PinValue.Low,
            [6] = PinValue.Low,
            [9] = PinValue.Low,
            [10] = PinValue.Low,
            [11] = PinValue.Low,
            [17] = PinValue.Low,
            [22] = PinValue.Low,
            [27] = PinValue.Low
        };

        public PiGpioService(ILogger<PiGpioService> logger)
        {
            this.logger = logger;

            var driver = new RaspberryPi3Driver();
            controller = new GpioController(PinNumberingScheme.Logical, driver);

            foreach (var (pin, value) in pins)
            {
                controller.OpenPin(pin, PinMode.Output);
                controller.Write(pin, value);
            }
        }

        public IReadOnlyDictionary<int, PinValue> Pins => pins;

        public void Dispose()
        {
            controller.Dispose();
        }

        public void Write(int pin, PinValue value)
        {
            logger.LogInformation($"Setting pin #{pin} to {value}");
            controller.Write(pin, value);
            pins[pin] = value;
        }
    }
}