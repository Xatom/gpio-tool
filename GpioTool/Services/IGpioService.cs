using System;
using System.Collections.Generic;
using System.Device.Gpio;

namespace GpioTool.Services
{
    public interface IGpioService : IDisposable
    {
        IReadOnlyDictionary<int, PinValue> Pins { get; }

        void Write(int pin, PinValue value);
    }
}